# motoko-crc
This repository is a series of [Motoko](https://sdk.dfinity.org/docs/language-guide/motoko.html) modules to calculate CRC checksum (8;16;32,64) to be used in [Dfinity](https://dfinity.org/) canister. These modules are largely inspired by the Rust library crc.

* [crates.io crc](https://crates.io/crates/crc)
* [github crc-rs](https://github.com/mrhooray/crc-rs) @mrhooray
* [github crc-catalog](https://github.com/akhilles/crc-catalog) @akhilles use as base for the script `./generate_catalog.sh`

Progress status:
* Algorithms catalog
    - [x] Code 
* CRC 16
    - [x] Code 
    - [x] Basic tests
    - [x] Automatic tests deploy and use __crc_lib_assets__ canister see code in __main.mo__
* CRC 8
    - [x] Code 
    - [x] Basic tests
    - [x] Automatic tests
* CRC 32
    - [x] Code 
    - [x] Basic tests
    - [x] Automatic tests
* CRC 64
    - [x] Code 
    - [x] Basic tests
    - [x] Automatic tests

# Samples

## imports

```motoko
    import Bool "mo:base/Bool";
    import Nat8 "mo:base/Nat8";
    import Nat16 "mo:base/Nat16";
    import Nat32 "mo:base/Nat32";
    import Nat64 "mo:base/Nat64";
    import Types "../crc/types";
    import CRC16 "../crc/crc16";
    import CRC8 "../crc/crc8";
    import CRC32 "../crc/crc32";
    import CRC64 "../crc/crc64";
```

## CRC16

```motoko
    public func sample (data : Blob) : async Nat16 {
        let crc = CRC16.Crc();
        crc.setAlgorithmByName("CRC_16_GSM"); // facultative otherwise it uses default
        return crc.checksum(data);
    };
```

```bash
dfx canister call crc_lib sample '((blob "abcd-123456"))';
```

## Others 

let crc = **CRC16**.Crc(); change with CRC8,CRC32,CRC64


# Versions

[Versions see CHANGELOG.md](https://gitlab.com/kurdy/motoko-crc/-/blob/main/CHANGELOG.md)

# Install

## Prerequisites

[Dfinity SDK - dfx](https://smartcontracts.org/docs/quickstart/local-quickstart.html) current version 0.12.1

**npm** - Install it according to your environment.

## Get & deploy

`git clone https://gitlab.com/kurdy/motoko-crc.git`

`cd motoko-crc`

`npm instal`

`dfx start --background`

`dfx deploy`

Command line to test CRC:

```bash
dfx canister call crc_lib listAlgorithmName;
dfx canister call crc_lib test_CRC32 '("123456789","CRC_32_BZIP2")';
dfx canister call crc_lib test_CRC64 '("123456789","CRC_64_XZ")';
dfx canister call crc_lib test_CRC16 '("123456789","CRC_16_XMODEM")';
dfx canister call crc_lib test_CRC8 '("123456789","CRC_8_BLUETOOTH")';
```

Or using Candid UI

open (http://127.0.0.1:8000/?canisterId={Candid_UI}&id={crc_lib}) replace with canisters ids

Example [http://127.0.0.1:8000/?canisterId=r7inp-6aaaa-aaaaa-aaabq-cai&id=rrkah-fqaaa-aaaaa-aaaaq-cai](http://127.0.0.1:8000/?canisterId=r7inp-6aaaa-aaaaa-aaabq-cai&id=rrkah-fqaaa-aaaaa-aaaaq-cai)

### Integrate into your project

copy _./src/crc_ folder into your _src_ folder

## How to get only crc modules

See [packages registries](https://gitlab.com/kurdy/motoko-crc/-/packages)

# References and bibliographies

* [In C/C++ what's the simplest way to reverse the order of bits in a byte?](https://stackoverflow.com/questions/2602823/in-c-c-whats-the-simplest-way-to-reverse-the-order-of-bits-in-a-byte#2602885)
* [CRC RevEng: arbitrary-precision CRC calculator and algorithm finder](https://reveng.sourceforge.io/)
* Online Tests
    * [crccalc](https://crccalc.com/)
    * [crc_js](http://www.sunshine2k.de/coding/javascript/crc/crc_js.html)
* wikipedia: [Computation of cyclic redundancy checks](https://en.wikipedia.org/wiki/Computation_of_cyclic_redundancy_checks)
* wikipedia: [Cyclic redundancy check](https://en.wikipedia.org/wiki/Cyclic_redundancy_check)