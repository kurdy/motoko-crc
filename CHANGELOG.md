0.0.1 Development version

1.0.0 First version CRC 8-16-32-64 are implemented with tests

1.0.1 Solve an issue in tests func test_CRCxx and add github build

1.0.2 Update to dfx 0.8.3

1.0.3 Update to dfx 0.8.4

1.0.4 Migrate to gitlab

1.0.5 Migrate to dfx 0.9.2

1.0.6 -

1.0.7 Migrate to dfx 0.10.1

1.0.8 Migrate to dfx 0.11.0

1.0.9 Migrate to dfx 0.11.1

1.0.10 Migrate to dfx 0.11.2

1.0.12 Migrate to dfx 0.12.1 and add crc64/redis