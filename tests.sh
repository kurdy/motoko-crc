#!/bin/sh
# Sample/memo command:
# D=$(reveng -m CRC-32/JAMCRC -X -c -f README.md);echo "obase=10; ibase=16; $D" |bc
# D=$(X=$(echo -n "abcdefghijklmnopqrstuvwxyz-ABCDEFGHIJKLMNOPQRSTUVWXYZ_0123456789" | xxd -p -);reveng -m CRC-16/M17 -c $X);echo $((16#$D))
# D=$(X=$(echo -n "abcdefghijklmnopqrstuvwxyz-ABCDEFGHIJKLMNOPQRSTUVWXYZ_0123456789" | xxd -p -);reveng -m CRC-16/M17 -X -c $X);echo "0x$D ";echo "obase=10; ibase=16; $D" |bc


status=$((0)) 

dfx canister call crc_lib_backend test_CRC8_setAlgorithm | grep OK
status=$((status + $?))

dfx canister call crc_lib_backend test_CRC8_findAlgorithmByName | grep OK
status=$((status + $?))

dfx canister call crc_lib_backend test_CRC8 '("123456789","CRC_8_BLUETOOTH")' | grep sum=38
status=$((status + $?))

dfx canister call crc_lib_backend test_CRC16_setAlgorithm | grep OK
status=$((status + $?))

dfx canister call crc_lib_backend test_CRC16_findAlgorithmByName | grep OK
status=$((status + $?))

dfx canister call crc_lib_backend test_CRC16 '("123456789","CRC_16_XMODEM")' | grep sum=12739
status=$((status + $?))

dfx canister call crc_lib_backend test_CRC32_setAlgorithm | grep OK
status=$((status + $?))

dfx canister call crc_lib_backend test_CRC32_findAlgorithmByName | grep OK
status=$((status + $?))

dfx canister call crc_lib_backend test_CRC32 '("123456789","CRC_32_BZIP2")' | grep sum=4236843288
status=$((status + $?))

dfx canister call crc_lib_backend test_CRC64_setAlgorithm | grep OK
status=$((status + $?))

dfx canister call crc_lib_backend test_CRC64_findAlgorithmByName | grep OK
status=$((status + $?))

dfx canister call crc_lib_backend test_CRC64 '("123456789","CRC_64_XZ")' | grep sum=11051210869376104954
status=$((status + $?))

# D=$(X=$(echo -n "abcdefghijklmnopqrstuvwxyz-ABCDEFGHIJKLMNOPQRSTUVWXYZ_0123456789" | xxd -p -);reveng -m CRC-16/M17 -c $X);echo $((16#$D))
dfx canister call crc_lib_backend test_CRC16 '("abcdefghijklmnopqrstuvwxyz-ABCDEFGHIJKLMNOPQRSTUVWXYZ_0123456789,‰‱➊➋➌➍➎➏➐➑➒➓","CRC_16_M17")' | grep sum=49842
status=$((status + $?))

dfx canister call crc_lib_backend test_CRC64 '("abcdefghijklmnopqrstuvwxyz-ABCDEFGHIJKLMNOPQRSTUVWXYZ_0123456789,‰‱➊➋➌➍➎➏➐➑➒➓","CRC_64_REDIS")' | grep sum=14508037051276851711
status=$((status + $?))

echo "Exit code: $status"

exit $((status + $?))