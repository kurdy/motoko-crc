import { crc_lib_backend as crc_lib } from "../../declarations/crc_lib_backend";

document.getElementById("clickMeBtn").addEventListener("click", async () => {
  const name = document.getElementById("name").value.toString();
  // Interact with crc_lib actor, calling the greet method
  const greeting = await crc_lib.greet(name);

  document.getElementById("greeting").innerText = greeting;
});
