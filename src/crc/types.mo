// MIT License Copyright (c) 2021 ʕʘ̅͜ʘ̅ʔ see file LICENSE
// https://gitlab.com/kurdy/motoko-crc
import Bool "mo:base/Bool";
import Nat16 "mo:base/Nat16";
import Nat32 "mo:base/Nat32";
import Nat64 "mo:base/Nat64";

/// Define type use by CRC. 
module {

    /// Define a CRC Algorithm 
    /// Info @ https://reveng.sourceforge.io/crc-catalogue/all.htm#crc.legend
    public type Algorithm <W> = {
        name : Text;
        poly : W;
        init : W;
        refin : Bool;
        refout : Bool;
        xorout : W;
        check : W;
        residue : W;
    };
    
};