// MIT License Copyright (c) 2021 ʕʘ̅͜ʘ̅ʔ see file LICENSE
// https://gitlab.com/kurdy/motoko-crc
import Bool "mo:base/Bool";
import Nat8 "mo:base/Nat8";
import Nat64 "mo:base/Nat64";
import Iter "mo:base/Iter";
import Array "mo:base/Array";
import Debug "mo:base/Debug";
import Option "mo:base/Option";
import Text "mo:base/Text";
import Types "./types";
import Utils "./utils";

/// Module use for CRC64 implementation
/// Howto see ./src/crc_lib/main.mo 
module {

    type Algorithm = Types.Algorithm<Nat64>;

    // ./generate_catalog.sh | grep CRC_64
    let Algorithms : [Algorithm] = [
        {name="CRC_64_ECMA_182";poly=0x42f0e1eba9ea3693; init=0x0; refin=false; refout=false; xorout=0x0; check=0x6c40df5f0b497347; residue=0x0},
        {name="CRC_64_GO_ISO";poly=0x000000000000001b; init=0xffffffffffffffff; refin=true; refout=true; xorout=0xffffffffffffffff; check=0xb90956c775a41001; residue=0x5300000000000000},
        {name="CRC_64_WE";poly=0x42f0e1eba9ea3693; init=0xffffffffffffffff; refin=false; refout=false; xorout=0xffffffffffffffff; check=0x62ec59e3f1a4f00a; residue=0xfcacbebd5931a992},
        {name="CRC_64_XZ";poly=0x42f0e1eba9ea3693; init=0xffffffffffffffff; refin=true; refout=true; xorout=0xffffffffffffffff; check=0x995dc9bbdf1939fa; residue=0x49958c9abd7d353f},
        {name="CRC_64_REDIS";poly=0xad93d23594c935a9; init=0x0; refin=true; refout=true; xorout=0x0; check=0xe9c6d914c4b8d9ca; residue=0x0}
    ];

    /// Get array of all CRC64 Algorithms
    public func getAlgorithms(): [Algorithm]{
        return Algorithms;
    };

    /// Find an Algorithm by its name
    /// Name: Algorithm name 
    public func findAlgorithmByName(name : Text) : ?Algorithm {
        return Array.find<Algorithm>(Algorithms,func(x){Text.equal(x.name,name)});
    }; 
    
    // Define a default Algorithm
    let defaulAlgorithm : Algorithm = {name="CRC_64_ECMA_182";poly=0x42f0e1eba9ea3693; init=0x0000000000000000; refin=false; refout=false; xorout=0x0000000000000000; check=0x6c40df5f0b497347; residue=0x0000000000000000};

    /// Class that implement Crc for Nat64
    public class Crc () {

        var algorithm : ?Algorithm = null;
        var table : ?[Nat64] = null;

        func crc64(poly: Nat64, reflect: Bool, byte: Nat8) : Nat64 {
            var b : Nat8 = byte;
            if (reflect) {
                b := Utils.reverse_bits_nat8(byte);
            };
            var value : Nat64 = Nat64.bitshiftLeft(Nat64.fromNat(Nat8.toNat(b)),56);

            for (i in Iter.range(0, 7)) {
                value := Nat64.bitxor(Nat64.bitshiftLeft(value,1),Nat64.bitshiftRight(value,63)*poly);
            };

            if (reflect) {
                value := Utils.reverse_bits_nat64(value);
            };
            return value;
        };

        func buildTable(poly: Nat64, reflect: Bool) : [Nat64] {
            var table = Array.init<Nat64>(256,0x0);
            for (i in Iter.range(0, 255)) {
                table[i] := crc64(poly, reflect, Nat8.fromNat(i));
            };
            return Array.freeze<Nat64>(table);
        };

        func tableEntry(index : Nat64) : Nat64 {
            let i : Nat = Nat64.toNat(Nat64.bitand(index,0xFF));
            let t : [Nat64] = Option.get(table,[]);
            return t[i];
        };

        /// Set all defaults if internal parameters are empty
        public func default() {
            if (Option.isNull(algorithm)) {
                Debug.print("Crc64: Set default algorithm");
                algorithm := ?defaulAlgorithm;
            };
            if (Option.isNull(table)) {
                Debug.print("Crc64: Create Table...");
                let algo = Option.get(algorithm,defaulAlgorithm);
                table := ?buildTable(algo.poly,algo.refin);
            };
        };

        /// Set an Algorithm see Types.Algorithm<W> 
        public func setAlgorithm(algo : Algorithm) {
            algorithm := ?algo;
            table := ?buildTable(algo.poly,algo.refin);
        };

        /// Set an Algorithm by its name 
        public func setAlgorithmByName(name : Text) {
            algorithm := findAlgorithmByName(name);
            switch (algorithm){
                case null algorithm:=?defaulAlgorithm;
                case (?algo) table := ?buildTable(algo.poly,algo.refin);
            };
        };

        /// Get current Algorithm if any
        public func getAlgorithm() : ?Algorithm {
            return algorithm;
        };

        /// Compute CRC for the Blob, if internal paramters are empty it use defaults
        public func checksum(data : Blob) : Nat64 {
            default();
            let algo = Option.get(algorithm,defaulAlgorithm);
            // init
            var crc = switch (algo.refin) {
                case true Utils.reverse_bits_nat64(algo.init);
                case false algo.init;
            };
            //Checksum
            if (algo.refin) {
                for (b in data.vals()) {
                    let te = tableEntry(Nat64.bitxor(crc,Nat64.fromNat(Nat8.toNat(b))));
                    crc := Nat64.bitxor(te,Nat64.bitshiftRight(crc,8));
                }
            } else {
                for (b in data.vals()) {
                    let te = tableEntry(Nat64.bitxor(Nat64.fromNat(Nat8.toNat(b)),Nat64.bitshiftRight(crc,56)));
                    crc := Nat64.bitxor(te,Nat64.bitshiftLeft(crc,8));
                }
            };

            // finalize
            if (Bool.logxor(algo.refin,algo.refout)) {
                crc := Utils.reverse_bits_nat64(crc);
            };
            return Nat64.bitxor(crc,algo.xorout);
        };

  };

};