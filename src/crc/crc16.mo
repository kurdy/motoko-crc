// MIT License Copyright (c) 2021 ʕʘ̅͜ʘ̅ʔ see file LICENSE
// https://gitlab.com/kurdy/motoko-crc
import Bool "mo:base/Bool";
import Nat8 "mo:base/Nat8";
import Nat16 "mo:base/Nat16";
import Nat32 "mo:base/Nat32";
import Nat64 "mo:base/Nat64";
import Iter "mo:base/Iter";
import Array "mo:base/Array";
import Debug "mo:base/Debug";
import Option "mo:base/Option";
import Text "mo:base/Text";
import Types "./types";
import Utils "./utils";

/// Module use for CRC16 implementation
/// Howto see ./src/crc_lib/main.mo 
module {

    type Algorithm = Types.Algorithm<Nat16>;

    // ./generate_catalog.sh | grep CRC_16
    let Algorithms : [Algorithm] = [
        {name="CRC_16_ARC";poly=0x8005; init=0x0000; refin=true; refout=true; xorout=0x0000; check=0xbb3d; residue=0x0000},
        {name="CRC_16_CDMA2000";poly=0xc867; init=0xffff; refin=false; refout=false; xorout=0x0000; check=0x4c06; residue=0x0000},
        {name="CRC_16_CMS";poly=0x8005; init=0xffff; refin=false; refout=false; xorout=0x0000; check=0xaee7; residue=0x0000},
        {name="CRC_16_DDS_110";poly=0x8005; init=0x800d; refin=false; refout=false; xorout=0x0000; check=0x9ecf; residue=0x0000},
        {name="CRC_16_DECT_R";poly=0x0589; init=0x0000; refin=false; refout=false; xorout=0x0001; check=0x007e; residue=0x0589},
        {name="CRC_16_DECT_X";poly=0x0589; init=0x0000; refin=false; refout=false; xorout=0x0000; check=0x007f; residue=0x0000},
        {name="CRC_16_DNP";poly=0x3d65; init=0x0000; refin=true; refout=true; xorout=0xffff; check=0xea82; residue=0x66c5},
        {name="CRC_16_EN_13757";poly=0x3d65; init=0x0000; refin=false; refout=false; xorout=0xffff; check=0xc2b7; residue=0xa366},
        {name="CRC_16_GENIBUS";poly=0x1021; init=0xffff; refin=false; refout=false; xorout=0xffff; check=0xd64e; residue=0x1d0f},
        {name="CRC_16_GSM";poly=0x1021; init=0x0000; refin=false; refout=false; xorout=0xffff; check=0xce3c; residue=0x1d0f},
        {name="CRC_16_IBM_3740";poly=0x1021; init=0xffff; refin=false; refout=false; xorout=0x0000; check=0x29b1; residue=0x0000},
        {name="CRC_16_IBM_SDLC";poly=0x1021; init=0xffff; refin=true; refout=true; xorout=0xffff; check=0x906e; residue=0xf0b8},
        {name="CRC_16_ISO_IEC_14443_3_A";poly=0x1021; init=0xc6c6; refin=true; refout=true; xorout=0x0000; check=0xbf05; residue=0x0000},
        {name="CRC_16_KERMIT";poly=0x1021; init=0x0000; refin=true; refout=true; xorout=0x0000; check=0x2189; residue=0x0000},
        {name="CRC_16_LJ1200";poly=0x6f63; init=0x0000; refin=false; refout=false; xorout=0x0000; check=0xbdf4; residue=0x0000},
        {name="CRC_16_MAXIM_DOW";poly=0x8005; init=0x0000; refin=true; refout=true; xorout=0xffff; check=0x44c2; residue=0xb001},
        {name="CRC_16_MCRF4XX";poly=0x1021; init=0xffff; refin=true; refout=true; xorout=0x0000; check=0x6f91; residue=0x0000},
        {name="CRC_16_MODBUS";poly=0x8005; init=0xffff; refin=true; refout=true; xorout=0x0000; check=0x4b37; residue=0x0000},
        {name="CRC_16_NRSC_5";poly=0x080b; init=0xffff; refin=true; refout=true; xorout=0x0000; check=0xa066; residue=0x0000},
        {name="CRC_16_OPENSAFETY_A";poly=0x5935; init=0x0000; refin=false; refout=false; xorout=0x0000; check=0x5d38; residue=0x0000},
        {name="CRC_16_OPENSAFETY_B";poly=0x755b; init=0x0000; refin=false; refout=false; xorout=0x0000; check=0x20fe; residue=0x0000},
        {name="CRC_16_PROFIBUS";poly=0x1dcf; init=0xffff; refin=false; refout=false; xorout=0xffff; check=0xa819; residue=0xe394},
        {name="CRC_16_RIELLO";poly=0x1021; init=0xb2aa; refin=true; refout=true; xorout=0x0000; check=0x63d0; residue=0x0000},
        {name="CRC_16_SPI_FUJITSU";poly=0x1021; init=0x1d0f; refin=false; refout=false; xorout=0x0000; check=0xe5cc; residue=0x0000},
        {name="CRC_16_T10_DIF";poly=0x8bb7; init=0x0000; refin=false; refout=false; xorout=0x0000; check=0xd0db; residue=0x0000},
        {name="CRC_16_TELEDISK";poly=0xa097; init=0x0000; refin=false; refout=false; xorout=0x0000; check=0x0fb3; residue=0x0000},
        {name="CRC_16_TMS37157";poly=0x1021; init=0x89ec; refin=true; refout=true; xorout=0x0000; check=0x26b1; residue=0x0000},
        {name="CRC_16_UMTS";poly=0x8005; init=0x0000; refin=false; refout=false; xorout=0x0000; check=0xfee8; residue=0x0000},
        {name="CRC_16_USB";poly=0x8005; init=0xffff; refin=true; refout=true; xorout=0xffff; check=0xb4c8; residue=0xb001},
        {name="CRC_16_XMODEM";poly=0x1021; init=0x0000; refin=false; refout=false; xorout=0x0000; check=0x31c3; residue=0x0000},
        {name="CRC_16_M17";poly=0x5935; init=0xffff; refin=false; refout=false; xorout=0x0000; check=0x772b; residue=0x0000}    
    ];

    /// Get array of all CRC16 Algorithms
    public func getAlgorithms(): [Algorithm]{
        return Algorithms;
    };

    /// Find an Algorithm by its name
    /// Name: Algorithm name
    public func findAlgorithmByName(name : Text) : ?Algorithm {
        return Array.find<Algorithm>(Algorithms,func(x){Text.equal(x.name,name)});
    }; 
    
     // Define a default Algorithm
    let defaulAlgorithm : Algorithm = {name="CRC_16_CDMA2000";poly=0xc867; init=0xffff; refin=false; refout=false; xorout=0x0000; check=0x4c06; residue=0x0000};

    /// Class thant implement Crc for Nat16
    public class Crc () {

        var algorithm : ?Algorithm = null;
        var table : ?[Nat16] = null;

        func crc16(poly: Nat16, reflect: Bool, byte: Nat8) : Nat16 {
            var b : Nat8 = byte;
            if (reflect) {
                b := Utils.reverse_bits_nat8(byte);
            };
            var value : Nat16 = Nat16.bitshiftLeft(Nat16.fromNat(Nat8.toNat(b)),8);

            for (i in Iter.range(0, 7)) {
                value := Nat16.bitxor(Nat16.bitshiftLeft(value,1),Nat16.bitshiftRight(value,15)*poly);
            };

            if (reflect) {
                value := Utils.reverse_bits_nat16(value);
            };
            return value;
        };

        func buildTable(poly: Nat16, reflect: Bool) : [Nat16] {
            var table = Array.init<Nat16>(256,0x0);
            for (i in Iter.range(0, 255)) {
                table[i] := crc16(poly, reflect, Nat8.fromNat(i));
            };
            return Array.freeze<Nat16>(table);
        };

        func tableEntry(index : Nat16) : Nat16 {
            let i : Nat = Nat16.toNat(Nat16.bitand(index,0x00FF));
            let t : [Nat16] = Option.get(table,[]);
            return t[i];
        };

        /// Set all defaults if internal parameters are empty
        public func default() {
            if (Option.isNull(algorithm)) {
                Debug.print("Crc16: Set default algorithm");
                algorithm := ?defaulAlgorithm;
            };
            if (Option.isNull(table)) {
                Debug.print("Crc16: Create Table...");
                let algo = Option.get(algorithm,defaulAlgorithm);
                table := ?buildTable(algo.poly,algo.refin);
            };
        };

        /// Set an Algorithm see Types.Algorithm<W> 
        public func setAlgorithm(algo : Algorithm) {
            algorithm := ?algo;
            table := ?buildTable(algo.poly,algo.refin);
        };

        /// Set an Algorithm by its name 
        public func setAlgorithmByName(name : Text) {
            algorithm := findAlgorithmByName(name);
            switch (algorithm){
                case null algorithm:=?defaulAlgorithm;
                case (?algo) table := ?buildTable(algo.poly,algo.refin);
            };
        };

        /// Get current Algorithm if any
        public func getAlgorithm() : ?Algorithm {
            return algorithm;
        };

        /// Compute CRC for the Blob, if internal paramters are empty it use defaults
        public func checksum(data : Blob) : Nat16 {
            default();
            let algo = Option.get(algorithm,defaulAlgorithm);
            // init
            var crc = switch (algo.refin) {
                case true Utils.reverse_bits_nat16(algo.init);
                case false algo.init;
            };
            //Checksum
            if (algo.refin) {
                for (b in data.vals()) {
                    let te = tableEntry(Nat16.bitxor(crc,Nat16.fromNat(Nat8.toNat(b))));
                    crc := Nat16.bitxor(te,Nat16.bitshiftRight(crc,8));
                }
            } else {
                for (b in data.vals()) {
                    let te = tableEntry(Nat16.bitxor(Nat16.fromNat(Nat8.toNat(b)),Nat16.bitshiftRight(crc,8)));
                    crc := Nat16.bitxor(te,Nat16.bitshiftLeft(crc,8));
                }
            };

            // finalize
            if (Bool.logxor(algo.refin,algo.refout)) {
                crc := Utils.reverse_bits_nat16(crc);
            };
            return Nat16.bitxor(crc,algo.xorout);
        };

        /*
        public func getTable() : [Nat16] {
            if (Option.isNull(table)) {
                default();
            };
            return Option.get(table,[]);
        };
        */

  };

};