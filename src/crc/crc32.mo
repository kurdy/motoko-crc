// MIT License Copyright (c) 2021 ʕʘ̅͜ʘ̅ʔ see file LICENSE
// https://gitlab.com/kurdy/motoko-crc
import Bool "mo:base/Bool";
import Nat8 "mo:base/Nat8";
import Nat32 "mo:base/Nat32";
import Iter "mo:base/Iter";
import Array "mo:base/Array";
import Debug "mo:base/Debug";
import Option "mo:base/Option";
import Text "mo:base/Text";
import Types "./types";
import Utils "./utils";

/// Module use for CRC32 implementation
/// Howto see ./src/crc_lib/main.mo 
module {

     type Algorithm = Types.Algorithm<Nat32>;

    // ./generate_catalog.sh | grep CRC_32
    let Algorithms : [Algorithm] = [
        {name="CRC_32_AIXM";poly=0x814141ab; init=0x00000000; refin=false; refout=false; xorout=0x00000000; check=0x3010bf7f; residue=0x00000000},
        {name="CRC_32_AUTOSAR";poly=0xf4acfb13; init=0xffffffff; refin=true; refout=true; xorout=0xffffffff; check=0x1697d06a; residue=0x904cddbf},
        {name="CRC_32_BASE91_D";poly=0xa833982b; init=0xffffffff; refin=true; refout=true; xorout=0xffffffff; check=0x87315576; residue=0x45270551},
        {name="CRC_32_BZIP2";poly=0x04c11db7; init=0xffffffff; refin=false; refout=false; xorout=0xffffffff; check=0xfc891918; residue=0xc704dd7b},
        {name="CRC_32_CD_ROM_EDC";poly=0x8001801b; init=0x00000000; refin=true; refout=true; xorout=0x00000000; check=0x6ec2edc4; residue=0x00000000},
        {name="CRC_32_CKSUM";poly=0x04c11db7; init=0x00000000; refin=false; refout=false; xorout=0xffffffff; check=0x765e7680; residue=0xc704dd7b},
        {name="CRC_32_ISCSI";poly=0x1edc6f41; init=0xffffffff; refin=true; refout=true; xorout=0xffffffff; check=0xe3069283; residue=0xb798b438},
        {name="CRC_32_ISO_HDLC";poly=0x04c11db7; init=0xffffffff; refin=true; refout=true; xorout=0xffffffff; check=0xcbf43926; residue=0xdebb20e3},
        {name="CRC_32_JAMCRC";poly=0x04c11db7; init=0xffffffff; refin=true; refout=true; xorout=0x00000000; check=0x340bc6d9; residue=0x00000000},
        {name="CRC_32_MPEG_2";poly=0x04c11db7; init=0xffffffff; refin=false; refout=false; xorout=0x00000000; check=0x0376e6e7; residue=0x00000000},
        {name="CRC_32_XFER";poly=0x000000af; init=0x00000000; refin=false; refout=false; xorout=0x00000000; check=0xbd0be338; residue=0x00000000}
    ];

    /// Get array of all CRC32 Algorithms
    public func getAlgorithms(): [Algorithm]{
        return Algorithms;
    };

    /// Find an Algorithm by its name
    /// Name: Algorithm name
    public func findAlgorithmByName(name : Text) : ?Algorithm {
        return Array.find<Algorithm>(Algorithms,func(x){Text.equal(x.name,name)});
    }; 
    
    // Define a default Algorithm
    let defaulAlgorithm : Algorithm = {name="CRC_32_BZIP2";poly=0x04c11db7; init=0xffffffff; refin=false; refout=false; xorout=0xffffffff; check=0xfc891918; residue=0xc704dd7b};

    /// Class thant implement Crc for Nat32
    public class Crc () {

        var algorithm : ?Algorithm = null;
        var table : ?[Nat32] = null;

        func crc32(poly: Nat32, reflect: Bool, byte: Nat8) : Nat32 {
            var b : Nat8 = byte;
            if (reflect) {
                b := Utils.reverse_bits_nat8(byte);
            };
            var value : Nat32 = Nat32.bitshiftLeft(Nat32.fromNat(Nat8.toNat(b)),24);

            for (i in Iter.range(0, 7)) {
                value := Nat32.bitxor(Nat32.bitshiftLeft(value,1),Nat32.bitshiftRight(value,31)*poly);
            };

            if (reflect) {
                value := Utils.reverse_bits_nat32(value);
            };
            return value;
        };

        func buildTable(poly: Nat32, reflect: Bool) : [Nat32] {
            var table = Array.init<Nat32>(256,0x0);
            for (i in Iter.range(0, 255)) {
                table[i] := crc32(poly, reflect, Nat8.fromNat(i));
            };
            return Array.freeze<Nat32>(table);
        };

        func tableEntry(index : Nat32) : Nat32 {
            let i : Nat = Nat32.toNat(Nat32.bitand(index,0xFF));
            let t : [Nat32] = Option.get(table,[]);
            return t[i];
        };

        /// Set all defaults if internal parameters are empty
        public func default() {
            if (Option.isNull(algorithm)) {
                Debug.print("Crc32: Set default algorithm");
                algorithm := ?defaulAlgorithm;
            };
            if (Option.isNull(table)) {
                Debug.print("Crc32: Create Table...");
                let algo = Option.get(algorithm,defaulAlgorithm);
                table := ?buildTable(algo.poly,algo.refin);
            };
        };

        /// Set an Algorithm see Types.Algorithm<W>
        public func setAlgorithm(algo : Algorithm) {
            algorithm := ?algo;
            table := ?buildTable(algo.poly,algo.refin);
        };

        /// Set an Algorithm by its name 
        public func setAlgorithmByName(name : Text) {
            algorithm := findAlgorithmByName(name);
            switch (algorithm){
                case null algorithm:=?defaulAlgorithm;
                case (?algo) table := ?buildTable(algo.poly,algo.refin);
            };
        };

        /// Get current Algorithm if any
        public func getAlgorithm() : ?Algorithm {
            return algorithm;
        };

        /// Compute CRC for the Blob, if internal paramters are empty it use defaults
        public func checksum(data : Blob) : Nat32 {
            default();
            let algo = Option.get(algorithm,defaulAlgorithm);
            // init
            var crc = switch (algo.refin) {
                case true Utils.reverse_bits_nat32(algo.init);
                case false algo.init;
            };
            //Checksum
            if (algo.refin) {
                for (b in data.vals()) {
                    let te = tableEntry(Nat32.bitxor(crc,Nat32.fromNat(Nat8.toNat(b))));
                    crc := Nat32.bitxor(te,Nat32.bitshiftRight(crc,8));
                }
            } else {
                for (b in data.vals()) {
                    let te = tableEntry(Nat32.bitxor(Nat32.fromNat(Nat8.toNat(b)),Nat32.bitshiftRight(crc,24)));
                    crc := Nat32.bitxor(te,Nat32.bitshiftLeft(crc,8));
                }
            };

            // finalize
            if (Bool.logxor(algo.refin,algo.refout)) {
                crc := Utils.reverse_bits_nat32(crc);
            };
            return Nat32.bitxor(crc,algo.xorout);
        };

  };

};