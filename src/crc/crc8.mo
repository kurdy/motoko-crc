// MIT License Copyright (c) 2021 ʕʘ̅͜ʘ̅ʔ see file LICENSE
// https://gitlab.com/kurdy/motoko-crc
import Bool "mo:base/Bool";
import Nat8 "mo:base/Nat8";
import Iter "mo:base/Iter";
import Array "mo:base/Array";
import Debug "mo:base/Debug";
import Option "mo:base/Option";
import Text "mo:base/Text";
import Types "./types";
import Utils "./utils";

/// Module use for CRC8 implementation
/// Howto see ./src/crc_lib/main.mo 
module {

    type Algorithm = Types.Algorithm<Nat8>;

    // ./generate_catalog.sh | grep CRC_8
    let Algorithms : [Algorithm] = [
        {name="CRC_8_AUTOSAR";poly=0x2f; init=0xff; refin=false; refout=false; xorout=0xff; check=0xdf; residue=0x42},
        {name="CRC_8_BLUETOOTH";poly=0xa7; init=0x00; refin=true; refout=true; xorout=0x00; check=0x26; residue=0x00},
        {name="CRC_8_CDMA2000";poly=0x9b; init=0xff; refin=false; refout=false; xorout=0x00; check=0xda; residue=0x00},
        {name="CRC_8_DARC";poly=0x39; init=0x00; refin=true; refout=true; xorout=0x00; check=0x15; residue=0x00},
        {name="CRC_8_DVB_S2";poly=0xd5; init=0x00; refin=false; refout=false; xorout=0x00; check=0xbc; residue=0x00},
        {name="CRC_8_GSM_A";poly=0x1d; init=0x00; refin=false; refout=false; xorout=0x00; check=0x37; residue=0x00},
        {name="CRC_8_GSM_B";poly=0x49; init=0x00; refin=false; refout=false; xorout=0xff; check=0x94; residue=0x53},
        {name="CRC_8_I_432_1";poly=0x07; init=0x00; refin=false; refout=false; xorout=0x55; check=0xa1; residue=0xac},
        {name="CRC_8_I_CODE";poly=0x1d; init=0xfd; refin=false; refout=false; xorout=0x00; check=0x7e; residue=0x00},
        {name="CRC_8_LTE";poly=0x9b; init=0x00; refin=false; refout=false; xorout=0x00; check=0xea; residue=0x00},
        {name="CRC_8_MAXIM_DOW";poly=0x31; init=0x00; refin=true; refout=true; xorout=0x00; check=0xa1; residue=0x00},
        {name="CRC_8_MIFARE_MAD";poly=0x1d; init=0xc7; refin=false; refout=false; xorout=0x00; check=0x99; residue=0x00},
        {name="CRC_8_NRSC_5";poly=0x31; init=0xff; refin=false; refout=false; xorout=0x00; check=0xf7; residue=0x00},
        {name="CRC_8_OPENSAFETY";poly=0x2f; init=0x00; refin=false; refout=false; xorout=0x00; check=0x3e; residue=0x00},
        {name="CRC_8_ROHC";poly=0x07; init=0xff; refin=true; refout=true; xorout=0x00; check=0xd0; residue=0x00},
        {name="CRC_8_SAE_J1850";poly=0x1d; init=0xff; refin=false; refout=false; xorout=0xff; check=0x4b; residue=0xc4},
        {name="CRC_8_SMBUS";poly=0x07; init=0x00; refin=false; refout=false; xorout=0x00; check=0xf4; residue=0x00},
        {name="CRC_8_TECH_3250";poly=0x1d; init=0xff; refin=true; refout=true; xorout=0x00; check=0x97; residue=0x00},
        {name="CRC_8_WCDMA";poly=0x9b; init=0x00; refin=true; refout=true; xorout=0x00; check=0x25; residue=0x00}
    ];

    /// Get array of all CRC8 Algorithms
    public func getAlgorithms(): [Algorithm]{
        return Algorithms;
    };

    /// Find an Algorithm by its name
    /// Name: Algorithm name 
    public func findAlgorithmByName(name : Text) : ?Algorithm {
        return Array.find<Algorithm>(Algorithms,func(x){Text.equal(x.name,name)});
    }; 
    
    // Define a default Algorithm
    let defaulAlgorithm : Algorithm = {name="CRC_8_CDMA2000";poly=0x9b; init=0xff; refin=false; refout=false; xorout=0x00; check=0xda; residue=0x00};

    /// Class that implement Crc for Nat8
    public class Crc () {

        var algorithm : ?Algorithm = null;
        var table : ?[Nat8] = null;

        func crc8(poly: Nat8, reflect: Bool, byte: Nat8) : Nat8 {
            var b : Nat8 = byte;
            if (reflect) {
                b := Utils.reverse_bits_nat8(byte);
            };
            var value : Nat8 = b;
            for (i in Iter.range(0, 7)) {
                value := Nat8.bitxor(Nat8.bitshiftLeft(value,1),Nat8.bitshiftRight(value,7)*poly);
            };

            if (reflect) {
                value := Utils.reverse_bits_nat8(value);
            };
            return value;
        };

        func buildTable(poly: Nat8, reflect: Bool) : [Nat8] {
            var table = Array.init<Nat8>(256,0x0);
            for (i in Iter.range(0, 255)) {
                table[i] := crc8(poly, reflect, Nat8.fromNat(i));
            };
            return Array.freeze<Nat8>(table);
        };

        func tableEntry(index : Nat8) : Nat8 {
            let i : Nat = Nat8.toNat(index);
            let t : [Nat8] = Option.get(table,[]);
            return t[i];
        };

        /// Set all defaults if internal parameters are empty
        public func default() {
            if (Option.isNull(algorithm)) {
                Debug.print("Crc8: Set default algorithm");
                algorithm := ?defaulAlgorithm;
            };
            if (Option.isNull(table)) {
                Debug.print("Crc8: Create Table...");
                let algo = Option.get(algorithm,defaulAlgorithm);
                table := ?buildTable(algo.poly,algo.refin);
            };
        };

        /// Set an Algorithm see Types.Algorithm<W> 
        public func setAlgorithm(algo : Algorithm) {
            algorithm := ?algo;
            table := ?buildTable(algo.poly,algo.refin);
        };

        /// Get current Algorithm if any
        public func getAlgorithm() : ?Algorithm {
            return algorithm;
        };

        /// Set an Algorithm by its name 
        public func setAlgorithmByName(name : Text) {
            algorithm := findAlgorithmByName(name);
            switch (algorithm){
                case null algorithm:=?defaulAlgorithm;
                case (?algo) table := ?buildTable(algo.poly,algo.refin);
            };
        };

        /// Compute CRC for the Blob, if internal paramters are empty it use defaults
        public func checksum(data : Blob) : Nat8 {
            default();
            let algo = Option.get(algorithm,defaulAlgorithm);
            // init
            var crc = switch (algo.refin) {
                case true Utils.reverse_bits_nat8(algo.init);
                case false algo.init;
            };
            //Checksum
            for (b in data.vals()) {
                crc := tableEntry(Nat8.bitxor(crc,b));
            };

            // finalize
            if (Bool.logxor(algo.refin,algo.refout)) {
                crc := Utils.reverse_bits_nat8(crc);
            };
            return Nat8.bitxor(crc,algo.xorout);
        };

  };

};