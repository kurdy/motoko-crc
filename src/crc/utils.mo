// MIT License Copyright (c) 2021 ʕʘ̅͜ʘ̅ʔ see file LICENSE
// hhttps://gitlab.com/kurdy/motoko-crc
import Nat8 "mo:base/Nat8";
import Nat16 "mo:base/Nat16";
import Nat32 "mo:base/Nat32";
import Nat64 "mo:base/Nat64";

/// Module that contains some utility functions
/// https://stackoverflow.com/questions/2602823/in-c-c-whats-the-simplest-way-to-reverse-the-order-of-bits-in-a-byte#2602885
/// Motoko playground https://m7sm4-2iaaa-aaaab-qabra-cai.raw.ic0.app/?tag=1747756211
module {
    
    /// Reverse bit on Nat8 
    /// 0xA0 (1010 0000) reverse → 0x05 (0000 0101)  
    public func reverse_bits_nat8(byte : Nat8) : Nat8 {
        var b : Nat8 = Nat8.bitor(Nat8.bitshiftRight(Nat8.bitand(byte,0xF0),4),Nat8.bitshiftLeft(Nat8.bitand(byte, 0x0F),4));
        b := Nat8.bitor(Nat8.bitshiftRight(Nat8.bitand(b,0xCC),2),Nat8.bitshiftLeft(Nat8.bitand(b, 0x33),2));
        b := Nat8.bitor(Nat8.bitshiftRight(Nat8.bitand(b,0xAA),1),Nat8.bitshiftLeft(Nat8.bitand(b, 0x55),1));
        return b;
    };

    /// Reverse bit on Nat16
    /// 0xB2C4 (1011 0010 1100 0100) → 0x234D (0010 0011 0100 1101) 
    public func reverse_bits_nat16(word : Nat16) : Nat16 { 
        var w : Nat16 = Nat16.bitor(Nat16.bitshiftRight(Nat16.bitand(word,0xFF00),8),Nat16.bitshiftLeft(Nat16.bitand(word, 0x00FF),8));
        w := Nat16.bitor(Nat16.bitshiftRight(Nat16.bitand(w,0xF0F0),4),Nat16.bitshiftLeft(Nat16.bitand(w, 0x0F0F),4));
        w := Nat16.bitor(Nat16.bitshiftRight(Nat16.bitand(w,0xCCCC),2),Nat16.bitshiftLeft(Nat16.bitand(w, 0x3333),2));
        w := Nat16.bitor(Nat16.bitshiftRight(Nat16.bitand(w,0xAAAA),1),Nat16.bitshiftLeft(Nat16.bitand(w, 0x5555),1));
        return w;
    };

    /// Reverse bit on Nat32 
    public func reverse_bits_nat32(dword : Nat32) : Nat32 { 
        var dw : Nat32 = Nat32.bitor(Nat32.bitshiftRight(Nat32.bitand(dword,0xFFFF0000),16),Nat32.bitshiftLeft(Nat32.bitand(dword, 0x0000FFFF),16));
        dw := Nat32.bitor(Nat32.bitshiftRight(Nat32.bitand(dw,0xFF00FF00),8),Nat32.bitshiftLeft(Nat32.bitand(dw, 0x00FF00FF),8));
        dw := Nat32.bitor(Nat32.bitshiftRight(Nat32.bitand(dw,0xF0F0F0F0),4),Nat32.bitshiftLeft(Nat32.bitand(dw, 0x0F0F0F0F),4));
        dw := Nat32.bitor(Nat32.bitshiftRight(Nat32.bitand(dw,0xCCCCCCCC),2),Nat32.bitshiftLeft(Nat32.bitand(dw, 0x33333333),2));
        dw := Nat32.bitor(Nat32.bitshiftRight(Nat32.bitand(dw,0xAAAAAAAA),1),Nat32.bitshiftLeft(Nat32.bitand(dw, 0x55555555),1));
        return dw;
    };

    /// Reverse bit on Nat64 
    public func reverse_bits_nat64(lword : Nat64) : Nat64 { 
        var lw : Nat64 = Nat64.bitor(Nat64.bitshiftRight(Nat64.bitand(lword,0xFFFFFFFF00000000),32),Nat64.bitshiftLeft(Nat64.bitand(lword, 0x00000000FFFFFFFF),32));
        lw := Nat64.bitor(Nat64.bitshiftRight(Nat64.bitand(lw,0xFFFF0000FFFF0000),16),Nat64.bitshiftLeft(Nat64.bitand(lw, 0x0000FFFF0000FFFF),16));
        lw := Nat64.bitor(Nat64.bitshiftRight(Nat64.bitand(lw,0xFF00FF00FF00FF00),8),Nat64.bitshiftLeft(Nat64.bitand(lw, 0x00FF00FF00FF00FF),8));
        lw := Nat64.bitor(Nat64.bitshiftRight(Nat64.bitand(lw,0xF0F0F0F0F0F0F0F0),4),Nat64.bitshiftLeft(Nat64.bitand(lw, 0x0F0F0F0F0F0F0F0F),4));
        lw := Nat64.bitor(Nat64.bitshiftRight(Nat64.bitand(lw,0xCCCCCCCCCCCCCCCC),2),Nat64.bitshiftLeft(Nat64.bitand(lw, 0x3333333333333333),2));
        lw := Nat64.bitor(Nat64.bitshiftRight(Nat64.bitand(lw,0xAAAAAAAAAAAAAAAA),1),Nat64.bitshiftLeft(Nat64.bitand(lw, 0x5555555555555555),1));
        return lw;
    };

};