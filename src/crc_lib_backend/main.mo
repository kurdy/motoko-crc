// MIT License Copyright (c) 2021 ʕʘ̅͜ʘ̅ʔ see file LICENSE
// https://github.com/rbolog/motoko-crc
import Bool "mo:base/Bool";
import Nat8 "mo:base/Nat8";
import Nat16 "mo:base/Nat16";
import Nat32 "mo:base/Nat32";
import Nat64 "mo:base/Nat64";
import Array "mo:base/Array";
import Iter "mo:base/Iter";
import Text "mo:base/Text";
import List "mo:base/List";
import Result "mo:base/Result";
import Types "../crc/types";
import CRC16 "../crc/crc16";
import CRC8 "../crc/crc8";
import CRC32 "../crc/crc32";
import CRC64 "../crc/crc64";
import EIC "mo:base/ExperimentalInternetComputer";

// Purpose: Tests lib and modules
actor {

    type Result<T,E> = Result.Result<T, E>;
    //let dataTest : Text ="abcdefghijklmnopqrstuvwxyz-ABCDEFGHIJKLMNOPQRSTUVWXYZ_0123456789,‰‱➊➋➌➍➎➏➐➑➒➓";
    let dataTest : Text ="123456789"; // Test value that should match with algorithm.check

    public func greet(name : Text) : async Text {
        let crc = CRC8.Crc();
        let result = crc.checksum(Text.encodeUtf8(name));
        return "CRC8 of " # name # " with CRC_8_CDMA2000=" # Nat8.toText(result);
    };

    public query func countInstructionsTestCRC32(text : Text) : async Nat64 {
        let crc = CRC32.Crc(); 
        let data = Text.encodeUtf8(text);
         EIC.countInstructions(func () {let _ = crc.checksum(data)});      
    };

    // CRC8 tests
    public func test_CRC8_setAlgorithm() : async [Text] {
        var result = List.nil<Text>();
        let crc = CRC8.Crc();
        let algorithms = CRC8.getAlgorithms();
        for (algorithm in algorithms.vals()) {
            crc.setAlgorithm(algorithm);
            var sum = crc.checksum(Text.encodeUtf8(dataTest));
            if (Nat8.equal(sum,algorithm.check)) {
                result := List.push(algorithm.name # " OK",result);
            } else {
                result := List.push(algorithm.name # " NOK sum=" # Nat8.toText(sum) #  " check=" # Nat8.toText(algorithm.check),result);
            };
        };
        return List.toArray(result);
    };

    public func test_CRC8_setAlgorithmByName() : async [Text] {
        var result = List.nil<Text>();
        let crc = CRC8.Crc();
        let algorithms = CRC8.getAlgorithms();
        for (algorithm in algorithms.vals()) {
            crc.setAlgorithmByName(algorithm.name);
            var sum = crc.checksum(Text.encodeUtf8(dataTest));
            if (Nat8.equal(sum,algorithm.check)) {
                result := List.push(algorithm.name # " OK",result);
            } else {
                result := List.push(algorithm.name # " NOK sum=" # Nat8.toText(sum) #  " check=" # Nat8.toText(algorithm.check),result);
            };
        };
        return List.toArray(result);
    };

    public func test_CRC8_findAlgorithmByName() : async [Text] {
        var result = List.nil<Text>();
        let crc = CRC8.Crc();
        let algorithms = CRC8.getAlgorithms();
        for (algorithm in algorithms.vals()) {
            crc.setAlgorithm(algorithm);
            let a = CRC8.findAlgorithmByName(algorithm.name);
            switch a {
                case (?algo) result := List.push(algo.name # " findAlgorithmByName->OK",result);
                case null result := List.push(algorithm.name # " findAlgorithmByName->NOK name=" # algorithm.name,result);
            };
        };
        return List.toArray(result);
    };

    public func test_CRC8(value: Text, algorithm : Text) : async Text {
        let crc = CRC8.Crc();
        crc.setAlgorithmByName(algorithm);
        let algoName = switch (crc.getAlgorithm()) {
            case null "No algorithm";
            case (?a) a.name;
        };
        return "sum=" # Nat8.toText(crc.checksum(Text.encodeUtf8(value))) # " algorithm=" # algoName;
    };


    // CRC16 tests
    public func test_CRC16_setAlgorithm() : async [Text] {
        var result = List.nil<Text>();
        let crc = CRC16.Crc();
        let algorithms = CRC16.getAlgorithms();
        for (algorithm in algorithms.vals()) {
            crc.setAlgorithm(algorithm);
            var sum = crc.checksum(Text.encodeUtf8(dataTest));
            if (Nat16.equal(sum,algorithm.check)) {
                result := List.push(algorithm.name # " OK",result);
            } else {
                result := List.push(algorithm.name # " NOK sum=" # Nat16.toText(sum) #  " check=" # Nat16.toText(algorithm.check),result);
            };
        };
        return List.toArray(result);
    };

    public func test_CRC16_setAlgorithmByName() : async [Text] {
        var result = List.nil<Text>();
        let crc = CRC16.Crc();
        let algorithms = CRC16.getAlgorithms();
        for (algorithm in algorithms.vals()) {
            crc.setAlgorithmByName(algorithm.name);
            var sum = crc.checksum(Text.encodeUtf8(dataTest));
            if (Nat16.equal(sum,algorithm.check)) {
                result := List.push(algorithm.name # " OK",result);
            } else {
                result := List.push(algorithm.name # " NOK sum=" # Nat16.toText(sum) #  " check=" # Nat16.toText(algorithm.check),result);
            };
        };
        return List.toArray(result);
    };

    public func test_CRC16_findAlgorithmByName() : async [Text] {
        var result = List.nil<Text>();
        let crc = CRC16.Crc();
        let algorithms = CRC16.getAlgorithms();
        for (algorithm in algorithms.vals()) {
            crc.setAlgorithm(algorithm);
            let a = CRC16.findAlgorithmByName(algorithm.name);
            switch a {
                case (?algo) result := List.push(algo.name # " findAlgorithmByName->OK",result);
                case null result := List.push(algorithm.name # " findAlgorithmByName->NOK name=" # algorithm.name,result);
            };
        };
        return List.toArray(result);
    };

    public func test_CRC16(value: Text, algorithm : Text) : async Text {
        let crc = CRC16.Crc();
        crc.setAlgorithmByName(algorithm);
        let algoName = switch (crc.getAlgorithm()) {
            case null "No algorithm";
            case (?a) a.name;
        };
        return "sum=" # Nat16.toText(crc.checksum(Text.encodeUtf8(value))) # " algorithm=" # algoName;
    };

    // CRC32 tests
    public func test_CRC32_setAlgorithm() : async [Text] {
        var result = List.nil<Text>();
        let crc = CRC32.Crc();
        let algorithms = CRC32.getAlgorithms();
        for (algorithm in algorithms.vals()) {
            crc.setAlgorithm(algorithm);
            var sum = crc.checksum(Text.encodeUtf8(dataTest));
            if (Nat32.equal(sum,algorithm.check)) {
                result := List.push(algorithm.name # " OK",result);
            } else {
                result := List.push(algorithm.name # " NOK sum=" # Nat32.toText(sum) #  " check=" # Nat32.toText(algorithm.check),result);
            };
        };
        return List.toArray(result);
    };

    public func test_CRC32_setAlgorithmByName() : async [Text] {
        var result = List.nil<Text>();
        let crc = CRC32.Crc();
        let algorithms = CRC32.getAlgorithms();
        for (algorithm in algorithms.vals()) {
            crc.setAlgorithmByName(algorithm.name);
            var sum = crc.checksum(Text.encodeUtf8(dataTest));
            if (Nat32.equal(sum,algorithm.check)) {
                result := List.push(algorithm.name # " OK",result);
            } else {
                result := List.push(algorithm.name # " NOK sum=" # Nat32.toText(sum) #  " check=" # Nat32.toText(algorithm.check),result);
            };
        };
        return List.toArray(result);
    };

    public func test_CRC32_findAlgorithmByName() : async [Text] {
        var result = List.nil<Text>();
        let crc = CRC32.Crc();
        let algorithms = CRC32.getAlgorithms();
        for (algorithm in algorithms.vals()) {
            crc.setAlgorithm(algorithm);
            let a = CRC32.findAlgorithmByName(algorithm.name);
            switch a {
                case (?algo) result := List.push(algo.name # " findAlgorithmByName->OK",result);
                case null result := List.push(algorithm.name # " findAlgorithmByName->NOK name=" # algorithm.name,result);
            };
        };
        return List.toArray(result);
    };

    public func test_CRC32(value: Text, algorithm : Text) : async Text {
        let crc = CRC32.Crc();
        crc.setAlgorithmByName(algorithm);
        let algoName = switch (crc.getAlgorithm()) {
            case null "No algorithm";
            case (?a) a.name;
        };
        return "sum=" # Nat32.toText(crc.checksum(Text.encodeUtf8(value))) # " algorithm=" # algoName;
    };

    // CRC64 tests
    public func test_CRC64_setAlgorithm() : async [Text] {
        var result = List.nil<Text>();
        let crc = CRC64.Crc();
        let algorithms = CRC64.getAlgorithms();
        for (algorithm in algorithms.vals()) {
            crc.setAlgorithm(algorithm);
            var sum = crc.checksum(Text.encodeUtf8(dataTest));
            if (Nat64.equal(sum,algorithm.check)) {
                result := List.push(algorithm.name # " OK",result);
            } else {
                result := List.push(algorithm.name # " NOK sum=" # Nat64.toText(sum) #  " check=" # Nat64.toText(algorithm.check),result);
            };
        };
        return List.toArray(result);
    };

    public func test_CRC64_setAlgorithmByName() : async [Text] {
        var result = List.nil<Text>();
        let crc = CRC64.Crc();
        let algorithms = CRC64.getAlgorithms();
        for (algorithm in algorithms.vals()) {
            crc.setAlgorithmByName(algorithm.name);
            var sum = crc.checksum(Text.encodeUtf8(dataTest));
            if (Nat64.equal(sum,algorithm.check)) {
                result := List.push(algorithm.name # " OK",result);
            } else {
                result := List.push(algorithm.name # " NOK sum=" # Nat64.toText(sum) #  " check=" # Nat64.toText(algorithm.check),result);
            };
        };
        return List.toArray(result);
    };

    public func test_CRC64_findAlgorithmByName() : async [Text] {
        var result = List.nil<Text>();
        let crc = CRC64.Crc();
        let algorithms = CRC64.getAlgorithms();
        for (algorithm in algorithms.vals()) {
            crc.setAlgorithm(algorithm);
            let a = CRC64.findAlgorithmByName(algorithm.name);
            switch a {
                case (?algo) result := List.push(algo.name # " findAlgorithmByName->OK",result);
                case null result := List.push(algorithm.name # " findAlgorithmByName->NOK name=" # algorithm.name,result);
            };
        };
        return List.toArray(result);
    };

    public func test_CRC64(value: Text, algorithm : Text) : async Text {
        let crc = CRC64.Crc();
        crc.setAlgorithmByName(algorithm);
        let algoName = switch (crc.getAlgorithm()) {
            case null "No algorithm";
            case (?a) a.name;
        };
        return "sum=" # Nat64.toText(crc.checksum(Text.encodeUtf8(value))) # " algorithm=" # algoName;
    };

    public func sample (data : Blob) : async Nat16 {
        let crc = CRC16.Crc();
        crc.setAlgorithmByName("CRC_16_GSM");
        return crc.checksum(data);
    };

    public query func listAlgorithmName()  : async [Text] {
        let crc8Algo = List.map<Types.Algorithm<Nat8>,Text>(List.fromArray(CRC8.getAlgorithms()), func(item){item.name});
        let crc16Algo = List.map<Types.Algorithm<Nat16>,Text>(List.fromArray(CRC16.getAlgorithms()), func(item){item.name});
        let crc32Algo = List.map<Types.Algorithm<Nat32>,Text>(List.fromArray(CRC32.getAlgorithms()), func(item){item.name});
        let crc64Algo = List.map<Types.Algorithm<Nat64>,Text>(List.fromArray(CRC64.getAlgorithms()), func(item){item.name});
        return List.toArray(List.append(List.append(crc8Algo,crc16Algo),List.append(crc32Algo,crc64Algo)));
    };

    /*
    public func getBlob () : async Blob {
        let data = Text.encodeUtf8("abcd-123456");
        return data;
    };  
    */
    
};