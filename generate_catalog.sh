#!/bin/sh
# This script is a clone copied from https://github.com/akhilles/crc-catalog . It has been adapted to generate a motoko catalog.

#echo "use crate::Algorithm;"
curl -s https://reveng.sourceforge.io/crc-catalogue/all.htm | grep -o 'width.*name.*"' | while read -r line; do
  # echo $(echo $line | \
  #   sed 's/ /, /g' | \
  #   sed 's/[-\/]/_/g' | \
  #   sed 's/width=\([0-9]*\), \(.*\), name="\(.*\)"/pub const \3: Algorithm<u\1> = Algorithm { \2 };/')
  
  width=$(echo $line | sed 's/width=\([0-9]*\) \(.*\) name="\(.*\)"/\1/')
  #params=$(echo $line | sed 's/width=\([0-9]*\) \(.*\) name="\(.*\)"/\2/' | sed 's/ /, /g' | sed 's/=/: /g')
  params=$(echo $line | sed 's/width=\([0-9]*\) \(.*\) name="\(.*\)"/\2/' | sed 's/ /; /g' | sed 's/=/=/g')
  name=$(echo $line | sed 's/width=\([0-9]*\) \(.*\) name="\(.*\)"/\3/' | sed 's/[-\/]/_/g')
  if [ $width -eq 8 ] || [ $width -eq 16 ] || [ $width -eq 32 ] || [ $width -eq 64 ]; then
    #echo "pub const $name: Algorithm<u$width> = Algorithm { $params };"
    #echo "public let $name : Algorithm<Nat$width> = {$params};"
    echo "{name=\"$name\";$params},"
  fi
done

# let Algorithms : [Algorithm<Nat8>] = [
#        {name = "CRC_8_AUTOSAR";poly=0x2f; init=0xff; refin=false; refout=false; xorout=0xff; check=0xdf; residue=0x42},
#        {name = "CRC_8_BLUETOOTH";poly=0xa7; init=0x00; refin=true; refout=true; xorout=0x00; check=0x26; residue=0x00}
#    ];